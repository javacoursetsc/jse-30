package ru.arubtsova.tm.exception.empty;

import ru.arubtsova.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error! Empty Password...");
    }

}
