package ru.arubtsova.tm.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
public abstract class AbstractBusinessEntity extends AbstractEntity {

    @Nullable
    protected String userId;

    @NotNull
    protected String name = "";

    @NotNull
    protected String description = "";

    @NotNull
    protected Status status = Status.NOT_STARTED;

    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    @Nullable
    protected Date dateStart;

    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    @Nullable
    protected Date dateFinish;

    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    @NotNull
    private Date created = new Date();

    @NotNull
    public String toString() {
        return id + ": " + name + " - " + description;
    }

}
