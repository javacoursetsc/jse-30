package ru.arubtsova.tm.api;

import ru.arubtsova.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

}
