package ru.arubtsova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.model.Project;
import ru.arubtsova.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    @NotNull
    List<Task> findAllTaskByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    Task bindTaskToProject(@Nullable String userId, @Nullable String taskId, @Nullable String projectId);

    @Nullable
    Task unbindTaskFromProject(@Nullable String userId, @Nullable String taskId);

    @Nullable
    Project removeProjectWithTasksById(@Nullable String userId, @Nullable String projectId);

}
