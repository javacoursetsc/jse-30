package ru.arubtsova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.IRepository;
import ru.arubtsova.tm.model.User;

import java.util.Optional;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    Optional<User> findByLogin(@NotNull String login);

    @NotNull
    Optional<User> findByEmail(@NotNull String email);

    @Nullable
    User removeByLogin(@NotNull String login);

}
